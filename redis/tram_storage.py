import asyncio
import aioredis

from app.exceptions import TramStateDoesNotExist
from app.serializers import TramState


class TramStorage:
    def __init__(self, redis: aioredis.Redis):
        self._redis = redis
        self._KEY = 'tram_{id}'

    async def get_state(self, tram_id: int) -> TramState:
        encoded_state = await self._redis.hgetall(self._KEY.format(id=tram_id))
        tram_state = {k.decode(): v.decode() for k, v in encoded_state.items()}

        if tram_state:
            return TramState(**tram_state)
        else:
            raise TramStateDoesNotExist

    async def set_state(self, tram_id: int, state: TramState):
        prepared_state = state.dict()

        await self._redis.publish_json(
            self._KEY.format(id=tram_id),
            prepared_state
        )

        await self._redis.hmset_dict(
            self._KEY.format(id=tram_id),
            prepared_state
        )

    async def get_current_stop_id(self, tram_id: int) -> int:
        stop = await self._redis.hmget(
            self._KEY.format(id=tram_id),
            'stop_id'
        )

        return int(stop[0].decode())


async def main():
    redis = await aioredis.create_redis_pool('redis://localhost')
    tram = TramStorage(redis)
    print(await tram.get_state(17))

    ch, = await redis.subscribe('test')
    print(await ch.get_json())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
