import asyncio
import logging

from aiohttp import web
from aioredis import Channel
from aiojobs.aiohttp import spawn


class TramProducer:
    def __init__(self, channel: Channel):
        self._channel = channel
        self._future: asyncio.Future = None

    def close(self):
        self._channel.close()

    def __aiter__(self):
        return self

    def __anext__(self):
        return asyncio.shield(self._get_message())

    async def _get_message(self):
        if self._future:
            return await self._future

        self._future = asyncio.get_event_loop().create_future()
        message = await self._channel.get_json()
        future, self._future = self._future, None
        future.set_result(message)

        return message


async def init_tram_channel(
        channel_name: str,
        app: web.Application
):
    if channel_name not in app['tram_producers']:
        channel, = await app['redis'].subscribe(channel_name)
        app['tram_producers'][channel_name] = TramProducer(channel)


async def run_tram_listening(
        request: web.Request,
        ws: web.WebSocketResponse,
        tram_producer: TramProducer
):
    """
    :return: aiojobs._job.Job object
    """
    listen_redis_job = await spawn(
        request,
        _read_tram_subscription(
            ws,
            tram_producer
        )
    )
    return listen_redis_job


async def _read_tram_subscription(
        ws: web.WebSocketResponse,
        tram_producer: TramProducer
):
    try:
        async for msg in tram_producer:
            await ws.send_json(msg)
    except asyncio.CancelledError:
        pass
    except Exception as e:
        logging.error(msg=e, exc_info=e)
