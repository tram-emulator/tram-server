import aioredis
from aioredis import Redis


async def create_redis(config) -> Redis:
    redis = await aioredis.create_redis_pool(
        f"redis://{config['host']}:{config['port']}"
    )

    return redis


async def close_redis(redis: Redis):
    redis.close()
    await redis.wait_closed()
