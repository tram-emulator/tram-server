import json
import aiomysql
import aioredis
import pydantic

from aiohttp import web
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiojobs import aiohttp
from asynctest import patch

import tests

from app import exceptions
from app.response import APIResponse
from app.start import init_mysql, init_redis
from app.routes import routes
from app.serializers import TramState
from app.middlewares import api_response


class ApplicationTest(AioHTTPTestCase):
    async def get_application(self):
        app = web.Application()
        app['config'] = tests.get_test_config()
        app.router.add_routes(routes)
        app.cleanup_ctx.extend([
            init_mysql,
            init_redis,
        ])
        aiohttp.setup(app)
        app.middlewares.append(api_response)

        return app


class TestStopsView(ApplicationTest):
    def setUp(self):
        super().setUp()

    @patch('app.views.stops.StopsRepository.get_by_route')
    @unittest_run_loop
    async def test_get_stops(self, get_stops_mock):
        route_id = 1
        get_stops_mock.return_value = []

        response = await self.client.get(
            self.app.router['stops'].url_for(route_id=str(route_id))
        )
        self.assertEqual(response.status, web.HTTPOk.status_code)

        data = await response.json()
        self.assertListEqual(get_stops_mock.return_value, data)

        get_stops_mock.assert_called_with(
            route_id,
            direction=None
        )

    @patch(
        'app.views.stops.StopsRepository.get_by_route',
        side_effect=aiomysql.DatabaseError
    )
    @unittest_run_loop
    async def test_invalid_get_stops(self, get_stops_mock):
        response = await self.client.get(
            self.app.router['stops'].url_for(route_id='0')
        )

        get_stops_mock.assert_called()

        data = await response.json()
        self.assertDictEqual(data, exceptions.APIError().to_dict())

    @patch(
        'app.views.stops.StopsRepository.get_by_route',
        side_effect=aiomysql.DatabaseError
    )
    @unittest_run_loop
    async def test_error_in_get_stops(self, get_stops_mock):
        response = await self.client.get(
            self.app.router['stops'].url_for(route_id='0')
        )
        get_stops_mock.assert_called()

        error = await response.json()
        self.assertDictEqual(
            error,
            exceptions.APIError().to_dict()
        )

    @patch('app.views.stops.StopsRepository.get_by_route')
    @unittest_run_loop
    async def test_get_stops_with_direction(self, get_stops_mock):
        fake_stops = []
        get_stops_mock.return_value = fake_stops
        direction = 'forward'
        route_id = 1

        response = await self.client.get(
            self.app.router['stops']
                .url_for(route_id=str(route_id))
                .with_query(f'direction={direction}')
        )

        stops = APIResponse(
            json.dumps(fake_stops).encode()
        ).to_json().decode()
        loaded_stops = await response.text()

        self.assertEqual(stops, loaded_stops)


class TestTramStateView(ApplicationTest):
    def setUp(self):
        super().setUp()

        self.tram_id = 1
        self.tram_state = TramState(**{
            'stop_id': 1,
            'direction': 'forward',
            'route_id': 1
        })

        self.invalid_tram_state = {
            'stop_id': 1,
            'direction': 'asasdasdasd',  # invalid direction
            'route_id': 1
        }

    @patch('app.views.tram.TramStorage.get_state')
    @unittest_run_loop
    async def test_get_tram_state(self, get_state_mock):
        get_state_mock.return_value = self.tram_state

        response = await self.client.get(
            self.app.router['tram_state'].url_for(tram_id=str(self.tram_id))
        )
        self.assertEqual(response.status, web.HTTPOk.status_code)
        get_state_mock.assert_called_with(self.tram_id)

        data = await response.json()
        self.assertDictEqual(self.tram_state.dict(), data)

    @patch(
        'app.views.tram.TramStorage.get_state',
        side_effect=exceptions.TramDoesNotExist
    )
    @unittest_run_loop
    async def test_get_nonexistent_state(self, get_state_mock):
        response = await self.client.get(
            self.app.router['tram_state'].url_for(tram_id=str(self.tram_id))
        )
        get_state_mock.assert_called_with(self.tram_id)

        error = await response.json()
        self.assertDictEqual(error, exceptions.TramDoesNotExist().to_dict())

    @patch(
        'app.views.tram.TramStorage.get_state',
        side_effect=aioredis.RedisError
    )
    @unittest_run_loop
    async def test_error_in_get_state(self, get_state_mock):
        response = await self.client.get(
            self.app.router['tram_state'].url_for(tram_id=str(self.tram_id))
        )
        get_state_mock.assert_called_with(self.tram_id)

        error = await response.json()
        self.assertDictEqual(
            error,
            exceptions.APIError().to_dict()
        )

    @patch('app.views.tram.TramStorage.set_state')
    @unittest_run_loop
    async def test_set_tram_state(self, set_state_mock):
        response = await self.client.put(
            self.app.router['tram_state'].url_for(tram_id=str(self.tram_id)),
            json=self.tram_state.dict()
        )
        self.assertEqual(response.status, web.HTTPOk.status_code)
        set_state_mock.assert_called_with(self.tram_id, self.tram_state)

    @unittest_run_loop
    async def test_set_invalid_state(self):
        response = await self.client.put(
            self.app.router['tram_state'].url_for(tram_id=str(self.tram_id)),
            json=self.invalid_tram_state
        )

        error = await response.json()
        try:
            TramState(**self.invalid_tram_state)
        except pydantic.ValidationError as e:
            self.assertEqual(
                json.dumps(exceptions.APIError(message=e.errors()).to_dict()),
                json.dumps(error)
            )

    @patch(
        'app.views.tram.spawn',
        side_effect=aioredis.RedisError
    )
    @unittest_run_loop
    async def test_error_in_set_state(self, spawn_mock):
        response = await self.client.get(
            self.app.router['tram_state'].url_for(tram_id=str(self.tram_id)),
            json=self.tram_state.dict()
        )

        error = await response.json()
        self.assertDictEqual(
            error,
            exceptions.TramStateDoesNotExist().to_dict()
        )


class TestTramListView(ApplicationTest):
    def setUp(self):
        super().setUp()

    @patch('app.views.tram.TramRepository.get_all')
    @unittest_run_loop
    async def test_tram_list(self, get_trams_mock):
        get_trams_mock.return_value = []
        response = await self.client.get(
            self.app.router['tram_list'].url_for()
        )

        self.assertEqual(response.status, web.HTTPOk.status_code)
        get_trams_mock.assert_called()

        data = await response.json()
        self.assertListEqual(get_trams_mock.return_value, data)
