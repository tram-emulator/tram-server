from uuid import uuid4
from aiohttp.test_utils import unittest_run_loop

from db.models import t_stops
from db.repository import StopsRepository
from db.entity_helpers import create_route
from app.exceptions import StopDoesNotExist
from tests.base_test_case import AsyncDBTestCase


class TestStopRepository(AsyncDBTestCase):
    @unittest_run_loop
    async def test_get_stop(self):
        async with self.engine.acquire() as conn:
            route_id = await create_route(conn, 'test')

            stop = {
                'route_id': route_id,
                'name': 'test',
                'direction': 'forward',
                'number': 1
            }

            result = await conn.execute(
                t_stops.insert().values(**stop)
            )

            stops_repository = StopsRepository(conn)
            created_stop = await stops_repository.get(result.lastrowid)
            created_stop.pop('id')

            self.assertDictEqual(stop, created_stop)

    @unittest_run_loop
    async def test_get_nonexistent_stop(self):
        with self.assertRaises(StopDoesNotExist):
            async with self.engine.acquire() as conn:
                stops_repository = StopsRepository(conn)
                await stops_repository.get(str(uuid4()))
