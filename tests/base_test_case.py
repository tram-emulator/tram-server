from unittest import TestCase
from aiohttp.test_utils import setup_test_loop, teardown_test_loop

import tests

from db.engine import close_mysql_engine
from db.test_utils import delete_data_from_tables
from app.start import create_mysql_engine
from redis.helpers import create_redis, close_redis


class AsyncTestCase(TestCase):
    def setUp(self):
        self.config = tests.get_test_config()
        self.loop = setup_test_loop()

    def tearDown(self):
        teardown_test_loop(self.loop)


class AsyncDBTestCase(AsyncTestCase):
    def setUp(self):
        super().setUp()

        self.engine = self.loop.run_until_complete(
            create_mysql_engine(self.config['mysql'])
        )

    def tearDown(self):
        self.loop.run_until_complete(
            delete_data_from_tables(self.engine)
        )

        self.loop.run_until_complete(
            close_mysql_engine(self.engine)
        )

        super().tearDown()


class AsyncRedisTestCase(AsyncTestCase):
    def setUp(self):
        super().setUp()

        redis_config = self.config['redis']
        self.redis = self.loop.run_until_complete(
            create_redis(redis_config)
        )

    def tearDown(self):
        self.loop.run_until_complete(
            close_redis(self.redis)
        )

        super().tearDown()
