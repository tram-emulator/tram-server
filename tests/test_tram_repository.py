from uuid import uuid4

from aiohttp.test_utils import unittest_run_loop

from app.exceptions import TramDoesNotExist
from db.models import t_trams
from db.repository import TramRepository
from db.entity_helpers import create_route
from tests.base_test_case import AsyncDBTestCase


class TestTramRepository(AsyncDBTestCase):
    @unittest_run_loop
    async def test_get_by_id(self):
        async with self.engine.acquire() as conn:
            tram_number = 343
            route_id = await create_route(conn, 'test')

            result = await conn.execute(
                t_trams.insert().values(
                    route_id=route_id, number=tram_number
                )
            )

            tram_id = result.lastrowid
            tram_repository = TramRepository(conn)

            tram = await tram_repository.get(tram_id)

            self.assertEqual(tram['number'], tram_number)
            self.assertEqual(tram['route_id'], route_id)

    @unittest_run_loop
    async def test_get_nonexistent_tram(self):
        async with self.engine.acquire() as conn:
            tram_id = str(uuid4())
            tram_repository = TramRepository(conn)
            with self.assertRaises(TramDoesNotExist):
                await tram_repository.get(tram_id)

    @unittest_run_loop
    async def test_create_tram(self):
        async with self.engine.acquire() as conn:
            number = 1
            route_id = await create_route(conn, 'test')

            tram_repository = TramRepository(conn)
            tram = await tram_repository.create(
                number=number,
                route_id=route_id
            )

            result = await conn.execute(t_trams.select().where(t_trams.c.id == tram['id']))
            row = await result.fetchone()
            created_tram = dict(row)

            self.assertEqual(created_tram['number'], number)
            self.assertEqual(created_tram['route_id'], route_id)

    @unittest_run_loop
    async def test_get_empty_tram_list(self):
        async with self.engine.acquire() as conn:
            tram_repository = TramRepository(conn)
            trams = await tram_repository.get_all()

            self.assertListEqual(trams, [])

    @unittest_run_loop
    async def test_get_tram_list(self):
        async with self.engine.acquire() as conn:
            route_id = await create_route(conn, 'test')

            tram_repository = TramRepository(conn)
            tram_1 = await tram_repository.create(number=1, route_id=route_id)
            tram_2 = await tram_repository.create(number=2, route_id=route_id)

            expected_trams = [tram_1, tram_2]
            loaded_trams = await tram_repository.get_all()

            self.assertListEqual(expected_trams, loaded_trams)
