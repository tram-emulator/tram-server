import json
import aiofiles

from os import path
from aiomysql.sa import SAConnection


async def create_stops_from_json(conn: SAConnection, stops: dict):
    stops = await load_stops()


async def load_stops():
    json_path = path.join(path.dirname(__file__), 'data/test_stops.json')

    async with aiofiles.open(json_path, mode='r') as f:
        return json.loads(await f.read())
