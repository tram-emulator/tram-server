from unittest import TestSuite

from app.start import read_config
from db.test_utils import drop_db, create_db

from tests.test_app import ApplicationTest
from tests.test_tram_repository import TestTramRepository
from tests.test_tram_storage import TestTramStorage


CONFIG = read_config()
test_db_name = f"test_{CONFIG['mysql']['db']}"
CONFIG['mysql']['db'] = test_db_name

TEST_CASES = (
    # ApplicationTest,
    TestTramRepository,
    # TestTramStorage,
)


def get_test_config():
    return CONFIG


def setup_tests():
    drop_db(CONFIG['mysql'])
    create_db(CONFIG['mysql'])


def load_tests(loader, tests, pattern):
    setup_tests()
    suite = TestSuite()

    for test_class in TEST_CASES:
        tests = loader.loadTestsFromTestCase(test_class)
        suite.addTests(tests)

    return suite
