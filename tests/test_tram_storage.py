from uuid import uuid4
from aiohttp.test_utils import unittest_run_loop

from app.serializers import TramState
from app.exceptions import TramStateDoesNotExist

from redis.tram_storage import TramStorage
from tests.base_test_case import AsyncRedisTestCase


class TestTramStorage(AsyncRedisTestCase):
    def setUp(self):
        super().setUp()

        self.tram_storage = TramStorage(self.redis)

        self.tram_id = str(uuid4())
        self.tram_key = f'tram_{self.tram_id}'
        self.tram_state = TramState(**dict(
            stop_id=1,
            route_id=1,
            direction='forward',
        ))

    @unittest_run_loop
    async def test_get_state(self):
        await self.redis.hmset_dict(
            self.tram_key,
            self.tram_state.dict()
        )

        loaded_tram_state = await self.tram_storage.get_state(self.tram_id)

        self.assertEqual(self.tram_state, loaded_tram_state)
        await self.redis.delete(self.tram_key)

    @unittest_run_loop
    async def test_set_state(self):
        await self.tram_storage.set_state(
            self.tram_id,
            self.tram_state
        )

        encoded_state = await self.redis.hgetall(self.tram_key)
        dict_tram_state = {k.decode(): v.decode() for k, v in encoded_state.items()}
        loaded_tram_state = TramState(**dict_tram_state)

        self.assertEqual(self.tram_state, loaded_tram_state)
        await self.redis.delete(self.tram_key)

    @unittest_run_loop
    async def test_get_current_stop(self):
        await self.tram_storage.set_state(
            self.tram_id,
            self.tram_state
        )

        stop_id = await self.tram_storage.get_current_stop_id(self.tram_id)
        self.assertEqual(self.tram_state.stop_id, stop_id)

        await self.redis.delete(self.tram_key)

    @unittest_run_loop
    async def test_get_nonexistent_state(self):
        with self.assertRaises(TramStateDoesNotExist):
            await self.tram_storage.get_state(str(uuid4()))
