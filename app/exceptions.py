class APIError(Exception):
    description: str = 'Internal server error'
    error_type: str

    def __init__(self, message: str = None):
        self.error_type = self.__class__.__name__
        if message:
            self.description = message

    def to_dict(self) -> dict:
        return dict(
            error_type=self.error_type,
            description=self.description
        )


class TramDoesNotExist(APIError):
    description = 'Failed to fetch tram'


class TramStateDoesNotExist(APIError):
    description = 'Failed to fetch tram state'


class StopDoesNotExist(APIError):
    description = 'Failed to fetch stop'
