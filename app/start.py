import logging
import weakref
import aiohttp_cors

from typing import Dict, AnyStr
from aiohttp import web, WSCloseCode
from aiojobs.aiohttp import setup
from configparser import ConfigParser
from logging.handlers import RotatingFileHandler

from app.views import *
from app.routes import routes
from app.settings import LOG_FILE, CONFIG_FILE
from app.middlewares import api_response

from db.engine import create_mysql_engine, close_mysql_engine
from redis.helpers import create_redis, close_redis
from redis.tram_channel import TramProducer


def create_app() -> web.Application:
    init_logging()

    app = web.Application()
    app['config'] = read_config()
    app.cleanup_ctx.extend([
        init_mysql,
        init_redis
    ])

    app['websockets'] = weakref.WeakSet()
    app['tram_producers']: Dict[AnyStr, TramProducer] = {}

    app.on_shutdown.extend([
        close_tram_producers,
        close_ws_connections
    ])

    app.router.add_routes(routes)
    app.middlewares.append(api_response)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    for route in app.router.routes():
        cors.add(route)

    setup(app)

    return app


def init_logging(file_name: str = LOG_FILE):
    log_file = RotatingFileHandler(
        filename=file_name,
        maxBytes=1024 * 1024,
        backupCount=3,
        encoding='utf-8'
    )

    log_file.setLevel(logging.INFO)

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s|tram_server|%(filename)20s[%(lineno)3s] %(levelname)8s : %(message)s',
        handlers=[log_file, console]
    )

    logging.basicConfig(level=logging.DEBUG)


def read_config(file_name: str = CONFIG_FILE):
    config = ConfigParser()
    config.read(file_name)

    return config


async def init_mysql(app: web.Application):
    db_config = app['config']['mysql']
    engine = await create_mysql_engine(db_config)
    app['db'] = engine

    yield
    await close_mysql_engine(app['db'])


async def init_redis(app: web.Application):
    config = app['config']['redis']
    redis = await create_redis(config)
    app['redis'] = redis

    yield
    await close_redis(app['redis'])


async def close_tram_producers(app: web.Application):
    for producer in app['tram_producers'].values():
        producer.close()


async def close_ws_connections(app: web.Application):
    for ws in set(app['websockets']):
        await ws.close(
            code=WSCloseCode.GOING_AWAY,
            message='Server shutdown'
        )
