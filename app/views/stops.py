import logging
import aiomysql

from aiohttp import web
from aiohttp.web_response import json_response
from aiohttp_cors import CorsViewMixin

from app.routes import routes
from app.exceptions import APIError
from db.repository.stops_repository import StopsRepository


__all__ = ['RouteStops']


@routes.view('/stops/{route_id}', name='stops')
class RouteStops(web.View, CorsViewMixin):
    async def get(self):
        try:
            route_id = int(self.request.match_info['route_id'])
            direction = self.request.rel_url.query.get('direction', None)

            async with self.request.app['db'].acquire() as conn:
                stops_repository = StopsRepository(conn)
                stops = await stops_repository.get_by_route(
                    route_id,
                    direction=direction
                )
                return json_response(data=stops)
        except aiomysql.DatabaseError as e:
            logging.exception(msg='Database error in fetching stops', exc_info=e)
            raise APIError
