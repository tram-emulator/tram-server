import logging
import asyncio
import aiohttp
import aiomysql
import aioredis
import pydantic

from aiohttp import web
from aiohttp.web_response import json_response
from aiojobs.aiohttp import spawn
from aiohttp_cors import CorsViewMixin

from app.routes import routes
from app.exceptions import APIError
from app.serializers import TramState

from db.repository import TramRepository
from redis.tram_storage import TramStorage
from redis.tram_channel import init_tram_channel, run_tram_listening


__all__ = [
    'tram_ws',
    'TramListView',
    'TramStateView',
]


@routes.view('/tram-state/{tram_id}', name='tram_state')
class TramStateView(web.View, CorsViewMixin):
    async def get(self):
        try:
            tram_id = int(self.request.match_info['tram_id'])
            tram_storage = TramStorage(self.request.app['redis'])
            tram_state = await tram_storage.get_state(tram_id)

            return json_response(data=tram_state.dict())
        except aioredis.RedisError as e:
            logging.exception(msg='Redis error in getting tram state', exc_info=e)
            raise APIError

    async def put(self):
        try:
            tram_id = int(self.request.match_info['tram_id'])
            tram_state = await self.request.json()
            tram_storage = TramStorage(self.request.app['redis'])
            await spawn(
                self.request,
                tram_storage.set_state(
                    tram_id,
                    TramState(**tram_state)
                )
            )
            logging.info(f"tram_{tram_id} has updated state: {tram_state}")

            return web.HTTPOk()
        except aioredis.RedisError as e:
            logging.exception(msg='Redis error in setting tram state', exc_info=e)
            raise APIError
        except pydantic.ValidationError as e:
            raise APIError(message=e.errors())


@routes.view('/tram-list', name='tram_list')
class TramListView(web.View, CorsViewMixin):
    async def get(self):
        try:
            db = self.request.app['db']
            async with db.acquire() as conn:
                tram_repository = TramRepository(conn)
                trams = await tram_repository.get_all()

                return json_response(data=trams)
        except aiomysql.DatabaseError as e:
            logging.exception(msg='Database error in getting tram list', exc_info=e)
            raise APIError


@routes.get('/tram-state-ws/{tram_id}')
async def tram_ws(request: web.Request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    tram_id = int(request.match_info['tram_id'])
    channel_name = f'tram_{tram_id}'

    await init_tram_channel(channel_name, request.app)
    tram_job = await run_tram_listening(
        request=request,
        ws=ws,
        tram_producer=request.app['tram_producers'][channel_name]
    )

    request.app['websockets'].add(ws)
    try:
        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == 'close':
                    await ws.close()
                    break
            if msg.type == aiohttp.WSMsgType.ERROR:
                logging.error(f'ws connection was closed with exception {ws.exception()}')
            else:
                await asyncio.sleep(0.005)
    except asyncio.CancelledError:
        pass
    finally:
        await tram_job.close()
        request.app['websockets'].discard(ws)

    return ws
