from enum import Enum
from pydantic import validator

from core.serializers import BaseSerializer


class Direction(str, Enum):
    Forward = 'forward'
    Backward = 'backward'


class TramState(BaseSerializer):
    stop_id: int = 0
    route_id: int = 0
    direction: str = ''

    @validator('direction')
    def validate_direction(cls, value):
        direction = Direction(value)
        return direction.value
