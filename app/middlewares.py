import logging

from aiohttp import web
from aiohttp.web_response import json_response

from app.response import APIResponse
from app.exceptions import APIError


@web.middleware
async def api_response(request: web.Request, handler):
    try:
        if isinstance(request, web.View):
            request = request.request
        response = await handler(request)

        if isinstance(response, web.WebSocketResponse):
            return response

        response.body = APIResponse(response.body).to_json()
        response.content_type = 'application/json'

        return response
    except APIError as e:
        return json_response(data=e.to_dict())
    except web.HTTPNotFound:
        error = APIError(message='Path not found')
        return json_response(data=error.to_dict())
    except Exception as e:
        logging.exception(msg='Server error', exc_info=e)
        error = APIError()
        return json_response(data=error.to_dict())
