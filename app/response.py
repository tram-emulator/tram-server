import json

from aiohttp import web


class APIResponse:
    data = {
        'status': web.HTTPOk.status_code,
        'message': 'OK',
        'data': None
    }

    def __init__(self, response_body: bytes):
        if is_json(response_body):
            self.data = json.loads(response_body.decode())
            self.body = json.dumps(self.data).encode()

    def to_json(self):
        return json.dumps(self.data).encode()

    def __dict__(self):
        return self.data


def is_json(body: bytes) -> bool:
    data = body.decode()

    try:
        json.loads(data)
        return True
    except json.JSONDecodeError:
        return False
