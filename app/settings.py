from os import path


PROJECT_DIR = path.dirname(path.dirname(__file__))
CONFIG_FILE = path.join(PROJECT_DIR, 'config.ini')
LOG_FILE = path.join(PROJECT_DIR, 'tram_server.log')
