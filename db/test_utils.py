from aiomysql.sa import Engine
from sqlalchemy import create_engine

from db.models import metadata, tables


def create_db(config):
    engine = create_engine(f"mysql+pymysql://{config['user']}:{config['password']}@{config['host']}:{config['port']}")
    engine.execute(f"CREATE DATABASE {config['db']}")

    engine = create_engine(
        f"mysql+pymysql://{config['user']}:{config['password']}@{config['host']}:{config['port']}/{config['db']}"
    )
    metadata.create_all(engine)


def drop_db(config):
    db_name = config['db']
    engine = create_engine(f"mysql+pymysql://{config['user']}:{config['password']}@{config['host']}:{config['port']}")
    engine.execute(F'DROP DATABASE IF EXISTS  {db_name}')


async def delete_data_from_tables(db: Engine):
    async with db.acquire() as conn:
        for table in tables:
            await conn.execute(table.delete())
