from aiomysql.sa import SAConnection

from db.models import t_routes


async def create_route(conn: SAConnection, name: str) -> int:
    result = await conn.execute(
        t_routes.insert().values(name=name)
    )

    return int(result.lastrowid)
