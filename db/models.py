# coding: utf-8
from sqlalchemy import Column, Enum, ForeignKey, String
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class Route(Base):
    __tablename__ = 'routes'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(50), nullable=False, unique=True)


t_routes = Route.__table__


class Stop(Base):
    __tablename__ = 'stops'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(50), nullable=False)
    number = Column(INTEGER(11), nullable=False)
    direction = Column(Enum('forward', 'backward'), nullable=False)
    route_id = Column(ForeignKey('routes.id'), nullable=False, index=True)

    route = relationship('Route')


t_stops = Stop.__table__


class Tram(Base):
    __tablename__ = 'trams'

    id = Column(INTEGER(11), primary_key=True)
    route_id = Column(ForeignKey('routes.id'), nullable=False, index=True)
    number = Column(INTEGER(11), nullable=False, unique=True)

    route = relationship('Route')


t_trams = Tram.__table__


tables = [
    t_trams,
    t_stops,
    t_routes,
]
