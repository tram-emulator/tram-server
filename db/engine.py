from aiomysql.sa import create_engine, Engine


async def create_mysql_engine(db_config) -> Engine:
    engine = await create_engine(
        host=db_config['host'],
        port=db_config.getint('port', 3306),
        user=db_config['user'],
        password=db_config['password'],
        db=db_config['db'],
        charset=db_config['charset'],
        use_unicode=db_config.getboolean('use_unicode', True),
        autocommit=db_config.getboolean('autocommit', True)
    )

    return engine


async def close_mysql_engine(engine: Engine):
    engine.close()
    await engine.wait_closed()
