from aiomysql.sa import SAConnection

from db.models import Stop, Route, t_stops
from db.repository.db_repository import DBRepository
from app.exceptions import StopDoesNotExist


routes = Route.__table__


class StopsRepository(DBRepository):
    def __init__(self, conn: SAConnection):
        super().__init__(conn, Stop.__table__)

    async def create(
            self,
            conn: SAConnection,
            name: str,
            number: int,
            route_id: int,
            direction: str
    ):
        cursor = await conn.execute(
            t_stops.insert().values(
                name=name,
                number=number,
                route_id=route_id,
                direction=direction
            )
        )
        await cursor.close()
        return await self.get(cursor.lastrowid)

    async def get(self, stop_id: int):
        cursor = await self._conn.execute(self._table.select().where(self._table.c.id == stop_id))
        row = await cursor.fetchone()
        await cursor.close()

        if row:
            return dict(row)
        else:
            raise StopDoesNotExist

    async def get_by_route(self, route_id: int, direction: str = None):
        query = self._table.select() \
            .where(self._table.c.route_id == route_id) \
            .order_by(self._table.c.direction) \
            .order_by(self._table.c.number)

        if direction:
            query = query.where(self._table.c.direction == direction)

        cursor = await self._conn.execute(query)
        return [dict(r) async for r in cursor]
