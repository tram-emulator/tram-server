import asyncio

from aiomysql.sa import SAConnection, create_engine

from db.models import Tram, Route
from db.repository.db_repository import DBRepository
from app.exceptions import TramDoesNotExist


routes = Route.__table__.c


class TramRepository(DBRepository):
    def __init__(self, conn: SAConnection):
        super().__init__(conn, Tram.__table__)

    async def get_all(self) -> list:
        query = self._table.select()\
            .where(self._table.c.route_id == routes.id)

        cursor = await self._conn.execute(query)
        return [dict(r) async for r in cursor]

    async def get(self, tram_id: int) -> dict:
        query = self._table.select() \
            .where(self._table.c.id == tram_id)

        cursor = await self._conn.execute(query)
        row = await cursor.fetchone()
        await cursor.close()

        if row:
            return dict(row)
        else:
            raise TramDoesNotExist

    async def create(self, route_id: int, number) -> dict:
        result = await self._conn.execute(
            self._table.insert().values(
                route_id=route_id,
                number=number
            )
        )
        return await self.get(result.lastrowid)


# async def main():
#     engine = await create_engine(
#         host='localhost',
#         port=3306,
#         user='root',
#         password='alex12245',
#         db='tram',
#         charset='utf8',
#         use_unicode=True,
#         autocommit=True
#     )
#
#     async with engine.acquire() as conn:
#         tram_repository = TramRepository(conn)
#         tram_list = await tram_repository.get_all()
#
#         for tram in tram_list:
#             print(tram)
#
#
# if __name__ == '__main__':
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(main())
