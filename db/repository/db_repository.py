from abc import ABC

from sqlalchemy import Table
from aiomysql.sa import SAConnection


class DBRepository(ABC):
    def __init__(self, conn: SAConnection, table: Table):
        self._conn = conn
        self._table = table
