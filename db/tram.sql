CREATE DATABASE tram CHARACTER SET utf8 COLLATE utf8_general_ci;

USE tram;


CREATE TABLE routes (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO 
	routes (name) 
VALUES ('4');


CREATE TABLE trams (
    id INT PRIMARY KEY AUTO_INCREMENT,
    route_id INT NOT NULL,
    number INT NOT NULL UNIQUE,
    FOREIGN KEY (route_id)
        REFERENCES routes (id)
);

INSERT INTO 
	trams (id, number, route_id)
VALUES (1, 577, 1);


CREATE TABLE stops (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    number INT NOT NULL,
    direction ENUM('forward', 'backward') NOT NULL,
    route_id INT NOT NULL,
    
    FOREIGN KEY (route_id)
        REFERENCES routes (id)
);

INSERT INTO 
	stops (name, number, direction, route_id)
VALUES 
	  ('ул. Сергея Слисаренко', 1, 'backward', 1),
    ('ул. Генерала Глаголева', 2, 'backward', 1),
    ('ул. Каштанов', 3, 'backward', 1),
    ('гостиница "Заря"', 4, 'backward', 1),
    ('ул. Спортивная', 5, 'backward', 1),
    ('ул. Москворецкая', 6, 'backward', 1),
    ('ЦУМ', 7, 'backward', 1),
    ('пл. Героев', 8, 'backward', 1),
    ('пл. ДМК', 9, 'backward', 1),
    
	  ('ул. Сергея Слисаренко', 9, 'forward', 1),
    ('ул. Генерала Глаголева', 8, 'forward', 1),
    ('ул. Каштанов', 7, 'forward', 1),
    ('гостиница "Заря"', 6, 'forward', 1),
    ('ул. Спортивная', 5, 'forward', 1),
    ('ул. Москворецкая', 4, 'forward', 1),
    ('пл. Героев', 3, 'forward', 1),
    ('"Прометей"', 2, 'forward', 1),
    ('пл. ДМК', 1, 'forward', 1);